FROM alpine

RUN apk add --no-cache \
	docker \
	git && \
    wget -O /usr/bin/linuxkit https://github.com/linuxkit/linuxkit/releases/download/v1.0.1/linuxkit-linux-amd64 && \
    chmod +x /usr/bin/linuxkit